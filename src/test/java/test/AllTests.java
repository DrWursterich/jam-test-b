package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import test.jam.test.b.MainTest;

@RunWith(Suite.class)
@SuiteClasses({ MainTest.class })
public class AllTests {

}

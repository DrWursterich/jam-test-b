package jam.test.b;

import jam.test.a.Printer;

public class Main {

	public static void main(final String... args) {
		final Printer printer = new Printer(System.out);
		printer.print("Hello, World!");
	}
}
